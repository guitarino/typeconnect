export interface IObserved<T> extends INode<T> {}

export interface IComputed<T> extends INode<T> {
	calculate: () => T;
	dependencies: INode<any>[];
}

export interface INode<T> {
	value: T;
	derivedNodes: IComputed<any>[];
	set(newValue: T): void;
	get(): T;
}

export type Configuration = {
	addPropertyNamesToNodes: boolean,
	addNodeLookup: boolean,
	setCallback: null | ((node: INode<unknown>, value: unknown) => any),
};
