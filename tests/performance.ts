import test from "ava";
import { connectFactory } from "./utils/api";
import { Fake, fake } from "./utils/fake";

type A = ReturnType<typeof createA>;
const createA = connectFactory(() => {
	return {
		a: Math.random(),
		b: Math.random(),
	}
});

type B = ReturnType<typeof createB>;
const createB = connectFactory((a: A) => {
	return {
		a: Math.random(),
		get b() {
			return this.a + a.a;
		},
		get c() {
			return this.b + a.b;
		}
	}
});

type C = ReturnType<typeof createC>;
const createC = connectFactory((a: A, b: B, fCall: Fake) => {
	return {
		a: Math.random(),
		get b() {
			return this.a + a.a;
		},
		get c() {
			return this.b + a.b;
		},
		get d() {
			return this.c + b.a;
		},
		get e() {
			return this.d + b.b;
		},
		get f() {
			fCall();
			return this.e + b.c;
		}
	}
});

type D = ReturnType<typeof createD>;
const createD = connectFactory((c: C) => {
	return {
		a: Math.random(),
		get b() {
			return this.a + c.e;
		},
		get c() {
			return this.b + c.f;
		},
	}
});

test(`Performance`, (t) => {
	const fCall = fake();
	const a = createA();
	let lastB = createB(a);
	let lastC = createC(a, lastB, fCall);
	for (let i = 0; i < 2200; i++) {
		lastB = createD(lastC);
		lastC = createC(lastB, lastC, fCall);
	}
	let result = lastC.f;
	let beforeUpdate = performance.now();
	console.log(`result before update = ${result}, calls = ${fCall.calls.length}`);
	a.a = Math.random();
	result = lastC.f;
	console.log(`result after update = ${result}, calls = ${fCall.calls.length}`);
	console.log(`duration = ${performance.now() - beforeUpdate} ms`);
	t.assert(true);
});
