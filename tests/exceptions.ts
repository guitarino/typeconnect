import test from "ava";
import { connect, updateManager } from "./utils/api";

updateManager.scheduleFunction = update => update();
updateManager.cancelFunction = () => {};

test(`Exceptions`, t => {
	const error = new Error('exception');

	const A = connect(class {
		a: number = 1;
		
		b: number = 2;
	
		get c(): number {
			if (this.a > 10) {
				throw error;
			}
			return this.a + this.b;
		}

		get d(): number {
			return this.c + 100;
		}
	});

	const a = new A();

	t.assert(a.d === 103, "Initial value is correct");

	t.throws(() => {
		a.a = 11;
	}, {
		is: error
	});

	t.assert(a.d === 103, "Further update is prevented");

	a.a = 10;

	t.assert(a.d === 112, "Update is resumed properly");
});