import { CyclicError, create, createUpdateManager } from '../../src/index';
import type { IComputed, IObserved } from '../../src/index';

export { CyclicError };

export type { IComputed, IObserved };

const updateManager = createUpdateManager();

const { 
	Computed,
	Observed,
	connect,
	connectFactory,
	connectObject,
	configureConnect,
	connectEffect,
} = create(updateManager);

export {
	Computed,
	Observed,
	connect,
	connectFactory,
	connectObject,
	configureConnect,
	connectEffect,
	updateManager,
}